﻿using System;
using TechTalk.SpecFlow;
using Newtonsoft.Json.Linq;
using OpenQA.Selenium;
using Skilfull.Tests.E2E.Datasources.Users;
using Skilfull.Tests.E2E.Helpers;
using System;
using System.Collections.Generic;
using System.Threading;
using TechTalk.SpecFlow;
using OpenQA.Selenium.Support.UI;
using Skilfull.Tests.E2E.Pages;
using NUnit.Framework;

namespace Skilfull.Tests.E2E.Steps.CRUD.Payments
{
    [Binding]
    public class UpdatePaymentDetailsSteps
    {
        private readonly ScenarioContext _scenarioContext;
        private readonly IWebDriver _webDriver;
        private readonly WebDriverWait _webDriverWait;
        protected string PaymentId;
        protected int CurrentPaymentValue;

        public UpdatePaymentDetailsSteps(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
            _webDriver = DriverManager.WebDriver;
            _webDriverWait = new WebDriverWait(_webDriver, DriverExtensions._defaultTimeout);

        }

        [Given(@"User Is Logged In and home page is opened")]
        public void GivenUserIsLoggedIn()
        {
           EnvironmentHelper.AssertUrl(_webDriver, PageUrls.HomePage);
         }

        [Given(@"User in Project Payments Page")]
        public void GivenUserInProjectPaymentsPage()
        {
            _webDriver.FindElement(HomePage.OpenNewProjectFile).Click();
            EnvironmentHelper.AssertUrl(_webDriver, PageUrls.ContractorProjectPayment);
           
        }

        [When(@"I Click On Modify For First Not Completed Payment")]
        public void WhenIClickOnModifyForFirstNotCompletedPayment()
        {
            try
            {
                _webDriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(PaymentsPage.firstModifyButton));
                PaymentId = _webDriver.FindElement(PaymentsPage.PaymentId).Text;
                PaymentId = PaymentId.Substring(PaymentId.LastIndexOf(':') + 1);
                _webDriver.FindElement(PaymentsPage.firstModifyButton).Click();
                EnvironmentHelper.Scroll(_webDriver);
            }
            catch{

                throw new ArgumentException("No Open Payments Found!");
            }
        }

        [Then(@"I Update Payment Value And Click save")]
        public void ThenIUpdatePaymentValueAndClickSave()
        {
            EnvironmentHelper.Scroll(_webDriver);
            _webDriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(PaymentsPage.breackDownModifyButton));
            _webDriver.FindElement(PaymentsPage.breackDownModifyButton).Click();
            _webDriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(PaymentsPage.PaymentPopUp));
           //get current value
            CurrentPaymentValue = int.Parse( _webDriver.FindElement(PaymentsPage.currentPaymentValue).GetProperty("value"));
            //update it
            _webDriver.FindElement(PaymentsPage.currentPaymentValue).Clear();
            _webDriver.FindElement(PaymentsPage.currentPaymentValue).SendKeys(EnvironmentHelper.getNewPaymentValue(CurrentPaymentValue));
            //save it
            _webDriver.FindElement(PaymentsPage.PopupSaveButton).Click();

        }

        [Then(@"I Assert That Response Message is : (.*)")]
        public void ThenIAssertThatResponseMessageIsUpdatedSuccessfully(string message)
        {
            _webDriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.InvisibilityOfElementWithText(ToasterAlert.ToasterMessage, message));
        }

        [Then(@"I Move To Next Section")]
        public void ThenIMoveToNextSection()
        {
            _webDriver.FindElement(PaymentsPage.Next).Click();
        }

        [Then(@"I Attach Required Files Then Click Finish")]
        public void ThenIAttachRequiredFilesThenClickFinish()
        {
        }
        
        [Then(@"I Verify That Status Changed To : Complete")]
        public void ThenIVerifyThatStatusChangedToComplete()
        {
        }
    }
}
