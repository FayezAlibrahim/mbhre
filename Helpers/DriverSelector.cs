using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Opera;
using Skilfull;
using Skilfull.Tests;
using Skilfull.Tests.E2E;
using Skilfull.Tests.E2E.Helpers;
using System;
using System.IO;

namespace Skilfull.Tests.E2E.Helpers
{
    static class DriverSelector
    {
        public static IWebDriver SelectDriverFromEnvironment()
        {
            return (EnvironmentHelper.GetSeleniumDriver()?.Trim()?.ToLower()) switch
            {
                "firefox_driver" => FirefoxDriver,
                "edge_driver" => EdgeDriver,
                "ie_driver" => InternetExplorerDriver,
                "opera_driver" => OperaDriver,
                _ => ChromeDriver,
            };
        }

        private static string DriversDirectoryPath => Path.Combine(Directory.GetCurrentDirectory(), "Drivers");

        private readonly static PageLoadStrategy _pageLoadStrategy = PageLoadStrategy.Eager;
        private static FirefoxDriver FirefoxDriver
        {
            get
            {
                var service = FirefoxDriverService.CreateDefaultService(DriversDirectoryPath);
                service.Host = "::1";

                var options = new FirefoxOptions
                {
                    PageLoadStrategy = _pageLoadStrategy,
                };

                return new FirefoxDriver(DriversDirectoryPath, options);
            }
        }
        private static EdgeDriver EdgeDriver
        {
            get
            {
                var options = new EdgeOptions
                {
                    PageLoadStrategy = _pageLoadStrategy,
                };

                return new EdgeDriver(DriversDirectoryPath, options);
            }
        }
        private static InternetExplorerDriver InternetExplorerDriver
        {
            get
            {
                var options = new InternetExplorerOptions
                {
                    PageLoadStrategy = _pageLoadStrategy,
                };

                return new InternetExplorerDriver(DriversDirectoryPath, options);
            }
        }
        private static OperaDriver OperaDriver
        {
            get
            {
                var options = new OperaOptions
                {
                    PageLoadStrategy = _pageLoadStrategy,
                };

                return new OperaDriver(DriversDirectoryPath, options);
            }
        }
        private static ChromeDriver ChromeDriver
        {
            get
            {
                var options = new ChromeOptions
                {
                    PageLoadStrategy = _pageLoadStrategy,
                };
                
                return new ChromeDriver(DriversDirectoryPath, options);
            }
        }
    }
}
