﻿using Skilfull.Tests.E2E.Datasources;
using Skilfull.Tests.E2E.Helpers;
using System;
using System.IO;

namespace Skilfull.Tests.E2E.Pages
{
    public class PageUrls
    {
        public static string StartPage => Combine("");
        public static string LoginPage => Combine("contractorLogin");
        public static string HomePage => Combine("contractorDashboard");
        public static string ContractorProjectPayment => Combine("projectsList");

        private static string Combine(string url)
            => Path.Combine(EnvironmentHelper.WebsiteRootPageUrl, url);
    }
}
