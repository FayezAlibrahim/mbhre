﻿@Authorized  @MBRHE_Test
    Feature: updatePaymentDetails
	Update Existing Payment Details Feature

@Payments @update
Scenario: Update Existing Payment Details
	Given User Is Logged In and home page is opened
	 And User in Project Payments Page
	When I Click On Modify For First Not Completed Payment
	Then I Update Payment Value And Click save
	And  I Assert That Response Message is : تم حفظ البيانات بنجاح
	Then I Move To Next Section
	Then I Attach Required Files Then Click Finish
	Then I Verify That Status Changed To : Complete

	