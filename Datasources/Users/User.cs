﻿using Skilfull;
using Skilfull.Tests;
using Skilfull.Tests.E2E;
using Skilfull.Tests.E2E.Datasources;
using Skilfull.Tests.E2E.Datasources.Users;
using Skilfull.Tests.E2E.Helpers;

namespace Skilfull.Tests.E2E.Datasources.Users
{
    public class User
    {
        public User(string name, string email, string password, Stage stage)
        {
            Name = name;
            Email = email;
            Password = password;
            Stage = stage;
        }

        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public Stage Stage { get; set; }
    }
}

