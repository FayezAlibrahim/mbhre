using FluentAssertions;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using Skilfull;
using Skilfull.Tests;
using Skilfull.Tests.E2E;
using Skilfull.Tests.E2E.Helpers;
using System;
using System.IO;
using System.Threading;

namespace Skilfull.Tests.E2E.Helpers
{
    public static class EnvironmentHelper
    {
        public static string WebsiteRootPageUrl => _stage switch
        {

            Stage.MBRHE_Test => "https://mbrhedoor.azurefd.net/",
            Stage.MBRHE_Staging => "https://mbrhedoor.azurefd.net/",
            Stage.MBRHE_Production => "https://mbrhedoor.azurefd.net/",

            _ => "UNKNOWN",
        };

        
        private static Stage _stage = Stage.Unknown;
        public static Stage CurrentStage => _stage;
        public static void SetStage(Stage stage) => _stage = stage;

        public static string GetSeleniumDriver()
            => Environment.GetEnvironmentVariable("SELENIUM_DRIVER");

        public static TimeSpan? GetTestWaitingTimeout()
        {
            var secondsSrt = Environment.GetEnvironmentVariable("TEST_RENDER_TIMEOUT");
            if (string.IsNullOrEmpty(secondsSrt)) return null;
            else return TimeSpan.FromSeconds(Convert.ToInt32(secondsSrt));
        }

        public static void NavigateAndAssertUrl(IWebDriver _webDriver, string url)
        {
            _webDriver.Navigate().GoToUrl(url);
            AssertUrl(_webDriver, url);
        }

        public static void AssertUrl(IWebDriver _webDriver, string url)
        {
            _webDriver.WaitUntilPageLoaded(url);
            _webDriver.Url.Should().StartWith(url);
        }

   
        public static void Scroll(IWebDriver _webDriver)
        {
            WebDriverWait _webDriverWait = new WebDriverWait(_webDriver, DriverExtensions._defaultTimeout);
            _webDriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(By.Id("container")));
            var js = (IJavaScriptExecutor)_webDriver;
           
            js.ExecuteScript("document.querySelector('#container').scrollIntoView();window.scrollTo(0, 500)");
        }

        public static string getNewPaymentValue(int oldValue) {
            //return (oldValue < 90 ? oldValue + 10 : 100).ToString();
            return "100";

        }

    }

    public enum Stage
    {
        Unknown,

        MBRHE_Test = 1,
        MBRHE_Staging = 2,
        MBRHE_Production = 3,

    }

}
