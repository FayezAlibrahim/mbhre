﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Skilfull;
using Skilfull.Tests;
using Skilfull.Tests.E2E;
using Skilfull.Tests.E2E.Datasources;
using Skilfull.Tests.E2E.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Skilfull.Tests.E2E.Datasources.Users
{
    public static class UsersData
    {
        private static IEnumerable<User> _users;
        private static IEnumerable<User> _data;

        private static IEnumerable<User> ImportUsers()
        {

            return File
                 .ReadAllLines(Path.Combine("Datasources", "Users", "users.csv"))
                 .Skip(1)
                 .Select(e => e.Split(','))
                 .Where(e => e.Length > 0)
                 .Select(
                 e => new User(
                     e[0].Trim(), // Name
                     e[1].Trim(), // Email
                     e[2].Trim(), // Password
                     (Stage)Enum.Parse(typeof(Stage), e[3]) // Stage
                     )
                 );
  
        }



        public static IEnumerable<User> SearchUsers(Stage stage, int take = 1, string name = "")
        {
            _users ??= ImportUsers();
            var query = _users
                .Where(e => e.Stage == stage);
    
            if (!string.IsNullOrWhiteSpace(name))
            {
                query = query
                    .Where(e => e.Name.Contains(name, StringComparison.OrdinalIgnoreCase));
            }

            return query
                .Take(take);
        }
    }
}