
namespace Skilfull.Tests.E2E.Helpers.Enums
{
	public enum ElementState
	{
		VISIBLE,
		NOT_VISIBLE,
		EXISTS,
		NOT_EXIST
	}
}