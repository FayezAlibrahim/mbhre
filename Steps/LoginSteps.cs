﻿using FluentAssertions;
using OpenQA.Selenium;
using Skilfull.Tests.E2E.Pages;
using System;
using TechTalk.SpecFlow;
using Skilfull.Tests.E2E.Helpers;
using NUnit.Framework;
using Skilfull.Tests.E2E.Datasources;
using System.Threading;
using OpenQA.Selenium.Interactions;
using Skilfull.Tests.E2E.Datasources.Users;
using System.Diagnostics;
using System.Collections;
using OpenQA.Selenium.Support.UI;

namespace Skilfull.Tests.E2E.Steps
{
    [Binding]
    public class LoginSteps
    {
        private readonly ScenarioContext _scenarioContext;
        private readonly IWebDriver _webDriver;

        public User[] users;
        public string[] tags;
        private readonly WebDriverWait _webDriverWait;


        public LoginSteps(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
            _webDriver = DriverManager.WebDriver;
            tags = _scenarioContext.ScenarioInfo.ScenarioAndFeatureTags;
            _webDriverWait = new WebDriverWait(_webDriver, DriverExtensions._defaultTimeout);

        }

     

       


























            [Then(@"the (.*) will be redirected to Login page")]
        public void ThenTheUserWillBeRedirectedToLoginPage(int Role)
        {
            _webDriver.Url.Should().StartWith("");
        }

        [Then(@"the (.*) will click on logout button")]
        public void ThenTheUserWillClickOnLogoutButton(string userrole)
        {
            Thread.Sleep(5000);
            By logoutSelector= By.ClassName("user-name");
            By logoutButtonSelector= By.LinkText("Log Off");
      
            _webDriver.FindElement(logoutSelector).Click();
            _webDriver.FindElement(logoutButtonSelector).Click();
        }


    }
}
