﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace Skilfull.Tests.E2E.Pages
{
    class PaymentsPage
    {
        public static By PaymentTitleSpan = By.XPath("//div[contains(@class,'service_title_wrapper')]/span[text()='طلبات دفعات لمشاريع قروض البناء']");
        public static By firstModifyButton = By.XPath("//span[text()='طلب فتح ملف مشروع منح صيانة' or text()='Open New Project File For Maintenance Grant']/ancestor::div[contains(@class,'col-md-12')]/following-sibling::div[contains(@class,'col-md-12')]/descendant::button[position()=1 and contains(@class,'modifiBtn') ][1]");
        public static By PaymentId = By.XPath("//span[text()='طلب فتح ملف مشروع منح صيانة' or text()='Open New Project File For Maintenance Grant']/ancestor::div[contains(@class,'col-md-12')]/following-sibling::div[contains(@class,'col-md-12')]/descendant::button[position()=1 and contains(@class,'modifiBtn') ][1]/ancestor::div[contains(@class,'service_contant_wrapper')]/div[1]/div[2]/h3");
        public static By breackDownModifyButton = By.XPath("//tbody/tr[1]/td[6]/button[contains(@class,'modifiBtn modify')]/i");
        public static By currentPaymentValue = By.XPath("//div[contains(@class,'modifyNewProjectPopup')]/descendant::input[@type='number']");
        public static By PaymentPopUp = By.XPath("//div[contains(@class,'modifyNewProjectPopup')]");
        public static By PopupSaveButton = By.XPath("//div[contains(@class,'modifyFormSectionBtnWrapper')]/button[1]");
        public static By Next = By.XPath("//button[text()='التالي'  or text()='Next']");


    }
}
