﻿using Skilfull.Tests.E2E.Datasources;
using Skilfull.Tests.E2E.Datasources.Users;
using Skilfull.Tests.E2E.Helpers;
using Skilfull.Tests.E2E.Steps;
using Skilfull.Tests.E2E.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using Skilfull.Tests.E2E.Steps.Helpers;

namespace Skilfull.Tests.E2E.Hooks
{
    [Binding]
    public class LoginHooks
    {
        [BeforeScenario("Authorized")]
        public static void LoginCustomer(ScenarioContext scenarioContext)
        {
            var customer = UsersData
                .SearchUsers(EnvironmentHelper.CurrentStage, 1)
                .First();

           LoginHelper.Login(DriverManager.WebDriver, customer);
        }

    }
}