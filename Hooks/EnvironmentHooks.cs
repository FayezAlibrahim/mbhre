﻿using Skilfull.Tests.E2E.Helpers;
using Skilfull.Tests.E2E.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace Skilfull.Tests.E2E.Hooks
{
    [Binding]
    public class EnvironmentHooks
    {
        [BeforeFeature(Order = -int.MaxValue)]
        public static void SetFeatureEnvironment(FeatureContext featureContext)
        {
            var stage = Stage.Unknown;

        var tags = featureContext.FeatureInfo.Tags;

            if (tags.Contains("MBRHE_Test")) stage = Stage.MBRHE_Test;
            else if (tags.Contains("MBRHE_Staging")) stage = Stage.MBRHE_Staging;
            else if (tags.Contains("MBRHE_Production")) stage = Stage.MBRHE_Production;
           

            EnvironmentHelper.SetStage(stage);
        }

        [BeforeScenario(Order = -int.MaxValue)]
        public static void SetScenarioEnvironment(ScenarioContext scenarioContext)
        {
            var stage = Stage.Unknown;
            var tags = scenarioContext.ScenarioInfo.Tags;
            if (tags.Contains("MBRHE_Test")) stage = Stage.MBRHE_Test;
            else if (tags.Contains("MBRHE_Staging")) stage = Stage.MBRHE_Staging;
            else if (tags.Contains("MBRHE_Production")) stage = Stage.MBRHE_Production;
           

            if (EnvironmentHelper.CurrentStage == Stage.Unknown)
            {
                  EnvironmentHelper.SetStage(stage);
            }
            }

        [AfterScenario(Order = -1)]
        public static void ScreenshotError(ScenarioContext scenarioContext)
        {
            if (scenarioContext.TestError != null)
                ScreenshotManager.TakeScreenshot(DriverManager.WebDriver, ScreenshotType.Error);
        }

        [AfterScenario(Order = 1000)]
        public static void CleanDriver(ScenarioContext scenarioContext)
        {
            DriverManager.Cleanup();
        }

    }
}
