﻿using FluentAssertions;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using Skilfull.Tests.E2E.Datasources;
using Skilfull.Tests.E2E.Datasources.Users;
using Skilfull.Tests.E2E.Helpers;
using Skilfull.Tests.E2E.Pages;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using TechTalk.SpecFlow;

namespace Skilfull.Tests.E2E.Steps.Helpers
{
    public static class LoginHelper
    {
        private static WebDriverWait _webDriverWait;

        public static void Login(IWebDriver driver, User user)
        {
            _webDriverWait = new WebDriverWait(driver, DriverExtensions._defaultTimeout);
            EnvironmentHelper.NavigateAndAssertUrl(driver,PageUrls.LoginPage);
            _webDriverWait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(LoginPage.userName));

            driver.FindElement(LoginPage.userName).SendKeys(user.Email);
            driver.FindElement(LoginPage.password).SendKeys(user.Password);
            driver.FindElement(LoginPage.submitButton).Click();
            EnvironmentHelper.AssertUrl(driver, PageUrls.HomePage);

        }
    }
}