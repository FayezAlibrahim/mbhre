﻿using OpenQA.Selenium;
using Skilfull.Tests.E2E.Datasources;
using Skilfull.Tests.E2E.Helpers;
using System;
using System.IO;
namespace Skilfull.Tests.E2E.Pages
{

    class LoginPage
    {
        public static By userName = By.Id("username");
        public static By password = By.Id("password");
        public static By submitButton = By.Id("btnsubmitContractor");
    }
}
